import React from 'react';
import {bindActionCreators} from 'redux';

import * as projectDetailsActions from './actions/ProjectDetailsActions';
import ProjectDetails from './ProjectDetails.js';
import {connect} from 'react-redux';


class ProjectDetailsContainer extends React.Component {
    constructor(props) {
      super(props);
      this.state = {project: null};
    }
    componentDidMount() {
      this.props.actions.fetchStarterProject();
    }
    render() {
        console.log(this.props);
        console.log(this.state);
        return (
            <div className="ProjectDetailsContainer-component">
                <hr/>
                <ProjectDetails project={this.props.project} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {project: state.project}
}

function mapDispatchToProps(dispatch) {
    return {actions: bindActionCreators(projectDetailsActions, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectDetailsContainer);
