import React, { Component } from 'react';
import "./ProjectsList.css";
import petey from './img/finals/portfolios/peter.png';
import bitcoinHashrate from './img/finals/portfolios/coinbase.png';
import PicUpLogo from './img/finals/portfolios/picup_logo.png';
import potatoWebsite from './img/finals/portfolios/sweetpotato.png';
import dayhoff from './img/finals/portfolios/dayhoff_portfolio.jpeg';
import thiswebsite from './img/finals/portfolios/srsbzns.jpg';
// import memez from './img/finals/portfolios/memez.png';
import wildwildwebLogo from './img/finals/portfolios/wildwildwebpage.png';
import thrillhouseradio from './img/finals/portfolios/thrillhouse.png';
import pleasetunein from './img/finals/portfolios/pti.png';
import avalonbh from './img/finals/portfolios/avalonbh.png';
import carliepetraitis from './img/finals/portfolios/carliepetraitis.png';


class ProjectsList extends Component {
    render() {
        return (
            <div>                
                <div id="Projects" className="big-box-projects">
                    <div className="project-description">
                        <h3>
                        Projects I've Done
                        </h3>
                    </div>
                    <div className="project-array">
                        <div className="box-project">
                            <div>
                                <a href="projects/PTI">
                                <p className="project-heading">Please Tune In Project Blog</p>
                                <p><em>A project blog for discussing web development, radio stations, music scene happenings, and digital legislation</em></p>
                                <img src={pleasetunein} alt="" />
                                <p><em>Using: Wordpress, HTML, CSS, JavaScript, PHP</em></p>
                                </a>
                            </div>
                        </div>
                        {/* <div className="box-project">
                            <div><a href="projects/PETE">
                                <p className="project-heading">Petey's Fansite</p>
                                <p><em>The first side project website I ever made. A portfolio for my dog, Petey. Made in AngularJS.</em></p>
                                <img src={petey} alt="" />
                                <p><em>Using: HTML, CSS, JavaScript, AngularJS</em></p>
                                </a>
                            </div>
                        </div> */}
                        <div className="box-project">
                            <div>
                                <a href="projects/WWW">
                                <p className="project-heading">Wild Wild Web Page</p>
                                <p><em>A wild, wild place where anything goes. Hackers (code writers) and painters alike are welcome to contribute their design.</em></p>
                                <img src={wildwildwebLogo} alt="" />
                                <p><em>Using: HTML, CSS, JavaScript</em></p>
                                </a>
                            </div>
                        </div>
                        {/* <div className="box-project">
                            <div>
                                <a href="projects/AVALONBH">
                                <p className="project-heading">Avalon Behavioral Health Website</p>
                                <p><em>A WordPress site I helped develop and made changes on.</em></p>
                                <img src={avalonbh} alt="" />
                                <p><em>Using: WordPress, HTML, CSS, JavaScript</em></p>
                                </a>
                            </div>
                        </div> */}
                        {/* <div className="box-project">
                            <div>
                                <a href="projects/CARCAR">
                                <p className="project-heading">Carlie Petraitis Health Blog</p>
                                <p><em>A WordPress site I developed for my sister.</em></p>
                                <img src={carliepetraitis} alt="" />
                                <p><em>Using: Wordpress, HTML, CSS, JavaScript, PHP</em></p>
                                </a>
                            </div>
                            <p>WordPress Theme Customizations</p>
                        </div> */}
                        
                        {/* <div className="box-project">
                            <div>
                                <a href="projects/RIP">
                                <p className="project-heading">RIP Vine Radio</p>
                                <p><em>What used to be an underground project called thrillhouse radio has become RipVine Radio, a parody of a parody.</em></p>
                                <img src={thrillhouseradio} alt="" />
                                <p><em>Using: HTML, CSS, JavaScript, Python, Java</em></p>
                                </a>
                            </div>
                        </div> */}
                        
                        {/* <div className="box-project">
                        <p>
                            <a href="projects/MEMEZ">
                                <p className="project-heading">MemeLockerz.com</p>
                                <p><em>Memesite to store your favorite memes inside</em></p>
                                <img src={memez} alt="" />
                                <p><em>JavaScript, ReactJS, HTML, CSS, PHP, Go</em></p>
                            </a>
                        </p>
                        </div> */}
                        {/* <div className="this-website box-project">
                            <div>
                                <a href="projects/THIS">
                                <p className="project-heading">JackPetraitis.com</p> 
                                <p><em>(This Website)</em></p>
                                <img src={thiswebsite} alt="" />
                                <p><em>Using: ReactJS, JavaScript, HTML, CSS, NodeJS, MySQL</em></p>
                                </a>
                            </div>
                        </div> */}
                        {/* <div className="box-project">   
                            <div>
                                <a href="projects/DAYH">
                                    <p className="project-heading">Dayhoff Fashion</p>
                                    <em>Online Store using WordPress</em>
                                    <img src={dayhoff} alt="" />
                                    <p><em>Using: WordPress, HTML, CSS, JavaScript, PHP</em></p>
                                </a>
                            </div>                     
                        </div> */}
                        {/* <div className="box-project">
                            <div>
                                <a href="projects/STOP">
                                    <p className="project-heading">Stop Sending Me Potatoes</p>
                                    <img src={potatoWebsite} alt="" />
                                    <p><em>Using: Drupal, HTML, CSS, JavaScript, MySQL, Meteor</em></p>
                                </a>
                            </div>
                            <p><em>a small recipe building app built in ember that uses handlebars to display data on screen. meteor runs the backend.</em></p>                        
                        </div> */}
                        {/* <div className="box-project">
                            <div>
                                <a href="projects/PICUP">
                                    <p className="project-heading">PicUp Studio</p>
                                    Professional Headshot Photography for the Gig Economy
                                    <img src={PicUpLogo} alt="" />
                                    <p><em>Using: JavaScript, ReactJS, NodeJS, MySQL, HTML, CSS</em></p>                  
                                </a>
                            </div>
                            <p><em>a small reactjs app that is active on the phone and on the net to connect photographers with professionals in need of headshots.</em></p>                        
                        </div> */}
                        {/* <div className="box-project">
                            <div>
                                <p className="project-heading">Bitcoin Mining Pricer Program</p>
                                <a href="projects/BEST">
                                    <em>Video Card Pricer and Product Finder</em>
                                    <img src={bitcoinHashrate} alt="" />
                                </a>
                                <p><em>Using: JavaScript, AngularJS, .NET, SQL, HTML, CSS</em></p>   
                            </div>
                        </div> */}
                        <div className="box-project">
                            <div>
                                <p className="project-heading">Sign Up For an Account Now To See More</p>
                                <a href="projects/BEST">
                                    <em>Sign Up For a FREE Account</em>
                                    
                                </a>
                                <p>Sign up for a free account now using Okta, or SSO through Facebook/Gmail/WhatHaveyou... 
                                    Gain access to see more projects I have done. You will also be able to find out what I am most 
                                    currently working on and what kind of behind the scenes diagrams, writings and thoughts go into it.
                                </p>
                                <p><em>Using: JavaScript, AngularJS, .NET, SQL, HTML, CSS</em></p>   
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        )
    }
}

export default ProjectsList;