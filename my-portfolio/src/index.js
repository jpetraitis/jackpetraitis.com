import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Routing from './routing';
import { Provider } from 'react-redux';
import configureStore from './store.js';

const store = configureStore(); 

ReactDOM.render(
    <Provider store={store}>
        <Routing />
    </Provider>,
    document.getElementById('root')
);
