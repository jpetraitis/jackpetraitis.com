import React, { Component } from 'react';

class Logout extends Component {
    render() {
        return(
            <div>
                You have been successfully logged out!
            </div>
        )
    }
}

export default Logout;