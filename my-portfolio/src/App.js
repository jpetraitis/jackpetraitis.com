import React, { Component } from 'react';
import './App.css';
import ProjectsList from './ProjectsList';
import BooksList from './BooksList';
import Resumes from './Resumes';
import AppFooter from './AppFooter';

class App extends Component {
  render() {
    return (
      <div className="App">        
        <Resumes />
        <ProjectsList />
        <BooksList />
        <AppFooter />
        <div>
          <p>
            
          </p>
        </div>
      </div>
    );
  }
}

export default App;
