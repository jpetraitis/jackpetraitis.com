import React, { Component } from 'react';
import './AppFooter.css';

export default class AppHeader extends Component {
    render() {
        return (
            <section id="footer">
                <div className="text-section">
                    <i>This site was brought to you by Jack P. Thank you for perusing it so thoroughly to have reached the bottom.</i>
                    If you speak and read another language and would like localization, sign up for an account, a contact form will show up here, and you
                    can request the language of your choice. If you are viewing this site and you have difficulty with the contrast please sign up for 
                    an account and let me know. I try to keep this site in accorandance with all accessibility needs. I am currently working on making
                    it better for read-alound TTS (Text-to-speech) webpage readers.
                ©2022 
                </div>
            </section>
        );
    }
}