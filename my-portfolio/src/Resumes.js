import React, { Component } from 'react';
import resume from './resumes/finals/jackpetraitis_resume.pdf';
import resumeIcon from './img/finals/icons/resume.png';
import { withAuth } from '@okta/okta-react';
import "./Resumes.css";

export default withAuth(class Resumes extends Component {
    constructor(props) {
        super(props);
        this.state = { authenticated: null };
        this.checkAuthentication = this.checkAuthentication.bind(this);
        this.checkAuthentication();
    }

    async checkAuthentication() {
        const authenticated = await this.props.auth.isAuthenticated();
        if (authenticated !== this.state.authenticated) {
          this.setState({ authenticated });
        }
    }

    componentDidUpdate() {
        this.checkAuthentication();
    }
    render() {
        if (this.state.authenticated === null){
            return null;
        } else if (this.state.authenticated === true) {
            return (
                <div className="box-resume">
                    <div className="resume book-description">
                        <h3>
                        Resumes
                        </h3>
                    </div>
                    <div className="resumes-container">
                        <div className="resume-single">
                        <div className="resumes">
                            <h5>My Software Engineer Resume</h5>
                            <p><em>For all software engineering requests. Click to open.</em></p>              
                        </div>
                        <div className="pictureParagraph">
                            <a href={resume}>
                            <img src={resumeIcon} alt="resume icon" />
                            </a>
                        </div>
                        </div>
                        {/* <div className="resumes">
                        <h5>My Systems Security Resume</h5>
                        <p><em>For all systems security engineering requests</em></p>
                        <p className="pictureParagraph"><a href="resume2.pdf"><img src={resumeIcon} alt="resume icon" /></a></p>
                        </div>
                        <div className="resumes">
                        <h5>My IT Support Resume</h5>
                        <p><em>For all IT Support and System Admin requests</em></p>
                        <p className="pictureParagraph"><a href="resume3.pdf"><img src={resumeIcon} alt="resume icon" /></a></p>
                        </div> */}
                    </div>
                </div>
            )
        } else {
            return (
                <div>
                    
                </div>
            )
        }
    }
})
