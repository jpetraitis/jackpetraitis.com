import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Security, SecureRoute, ImplicitCallback } from '@okta/okta-react';
import App from "./App";
import ProjectDetails from "./ProjectDetails";
import BooksList from "./BooksList";
import ProjectsList from "./ProjectsList";
import SecureProtected from "./SecureProtected";
import Login from "./Login";
import AppHeader from "./AppHeader";
import RegistrationForm from "./RegistrationForm/RegistrationForm";
import Logout from "./Logout/Logout";

import config from "./config/app.config";

function onAuthRequired({history}) {
    history.push('/login');
}

function Routing() {
    return (
        <Router>
            <div>
                <Security issuer={config.oktaissuer}
                  clientId={config.clientid}
                  redirectUri={config.redirecturi}
                  onAuthRequired={onAuthRequired}
                    pkce={true} >
                    <AppHeader/>
                    <Route exact path="/" component={App} />
                    <SecureRoute path="/protected" component={SecureProtected} />
                    <Route path="/login" component={Login} />
                    <Route path="/logout" component={Logout} />
                    <Route path="/signup" component={RegistrationForm} />
                    <Route path="/projects/:id" component={ProjectDetails} />
                    <Route path="/books" component={BooksList} />
                    <Route exact path="/projects" component={ProjectsList} />
                    <Route path='/implicit/callback' component={ImplicitCallback} />
                </Security>
            </div>
        </Router>
    );
}


export default Routing;