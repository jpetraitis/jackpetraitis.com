export default {
    test: {
        name: "fake test name"
    },
    projects: [{
        id: "PETE",
        name: "Petey's Fansite",
        shortdetails: "The first side project website I ever made. A portfolio for my dog, Petey. Made in AngularJS.",
        imgsrc: ['./static/media/peter.a914718d.png'],
        techstack: "AngularJS, JavaScript, HTML, CSS",
        github: "https://github.com/jackpetraitis/yournewworldleader",
        url: "http://yournewworldleader.com",
        longdetails: "Petey's Fansite started off as a simple Angular1.6 app. I created it to learn more about Angular1.6 as I needed to use it for work. My idea was to create a fake online profile for my dog and make it seem like he was bent on ruling the world, kinda like 'The Brain' from Pinky and the Brain. No worries, as Petey doesn't have much of a brain. I used this learning experience to make a bunch of jokes in the persona of Petey being an evil doggy Emporer who is consumed with the evils of ruling his doggy nation."
    },{
        id: "AVALONBH",
        name: "Avalon Behavioral Health",
        shortdetails: "A WordPress site I helped develop and made changes on.",
        imgsrc: "../img/finals/portfolios/peter.png",
        techstack: "WordPress, HTML, CSS, JavaScript",
        github: "",
        url: "http://avalonbh.com",
        longdetails: "A client I worked for that had me fix some of there broken WordPress pages. I made it easier for them to update their website."
    },{
        id: "CARCAR",
        name: "Carlie Petraitis Health Blog",
        shortdetails: "A WordPress site I developed for my sister.",
        imgsrc: "../img/finals/portfolios/peter.png",
        techstack: "Wordpress, HTML, CSS, JavaScript, PHP",
        github: "https://github.com/jackpetraitis/yournewworldleader",
        url: "https://carliepetraitis.com/",
        longdetails: ""
    },{
        id: "PTI",
        name: "Please Tune In Project Blog",
        shortdetails: "A project blog for discussing web development, radio stations, music scene happenings, and digital legislation.",
        imgsrc: "../img/finals/portfolios/peter.png",
        techstack: "Wordpress, HTML, CSS, JavaScript, PHP, MySQL",
        github: "",
        url: "https://pleasetunein.com",
        longdetails: "."
    },{
        id: "RIP",
        name: "RIP Vine Radio",
        shortdetails: "What used to be an underground project called thrillhouse radio has become RipVine Radio, a parody of a parody.",
        imgsrc: "../img/finals/portfolios/peter.png",
        techstack: "HTML, CSS, JavaScript, Python, Java",
        github: "",
        url: "https://pleasetunein.com",
        longdetails: "."
    },{
        id: "WWW",
        name: "Wild Wild Web Page",
        shortdetails: "A wild, wild place where anything goes. Hackers (code writers) and painters alike are welcome to contribute their design.",
        imgsrc: "../img/finals/portfolios/peter.png",
        techstack: "HTML, CSS, JavaScript, PHP",
        github: "",
        url: "https://wildwildweb.page",
        longdetails: "."
    },{
        id: "MEMEZ",
        name: "MemeLockerz.com",
        shortdetails: "Memesite to store your favorite memes inside.",
        imgsrc: "../img/finals/portfolios/peter.png",
        techstack: "JavaScript, ReactJS, HTML, CSS, PHP, Go",
        github: "",
        url: "https://wildwildweb.page",
        longdetails: "."
    },{
        id: "THIS",
        name: "JackPetraitis.com",
        shortdetails: "(This Website)",
        imgsrc: "../img/finals/portfolios/peter.png",
        techstack: "JavaScript, ReactJS, HTML, CSS, PHP, Go",
        github: "",
        url: "https://www.jackpetraitis.com",
        longdetails: "."
    },{
        id: "DAYH",
        name: "Richard Dayhoff Fashion",
        shortdetails: "Online Store using WordPress",
        imgsrc: "../img/finals/portfolios/peter.png",
        techstack: "WordPress, HTML, CSS, JavaScript, PHP",
        github: "",
        url: "http://www.richarddayhoff.com/",
        longdetails: "."
    },{
        id: "STOP",
        name: "Stop Sending Me Potatoes",
        shortdetails: "a small recipe building app built in ember that uses handlebars to display data on screen. meteor runs the backend.",
        imgsrc: "../img/finals/portfolios/peter.png",
        techstack: "Drupal, HTML, CSS, JavaScript, MySQL, Meteor",
        github: "",
        url: "http://www.stopsendingmepotatoes.com/",
        longdetails: "."
    },{
        id: "PICUP",
        name: "PicUp Studio",
        shortdetails: "Professional Headshot Photography for the Gig Economy",
        imgsrc: "../img/finals/portfolios/peter.png",
        techstack: "JavaScript, ReactJS, NodeJS, MySQL, HTML, CSS",
        github: "",
        url: "http://www.picup.com/",
        longdetails: "."
    },{
        id: "BEST",
        name: "Bitcoin Mining Pricer Program",
        shortdetails: "Video Card Pricer and Product Finder",
        imgsrc: "../img/finals/portfolios/peter.png",
        techstack: "JavaScript, ReactJS, NodeJS, MySQL, HTML, CSS",
        github: "https://github.com/jackpetraitis/besthashfordacash",
        url: "",
        longdetails: "."
    }]
}