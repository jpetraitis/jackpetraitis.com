import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function testReducer (state = initialState, action) {
    switch(action.type) {
        case types.FETCH_TEST_SUCCESS:
            return action.test;
        default:
            return state;
    }
}