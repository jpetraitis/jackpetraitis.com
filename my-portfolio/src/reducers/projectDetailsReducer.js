import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function projectDetailsReducer (state = initialState, action) {
    switch(action.type) {
        case types.FETCH_PROJECTS_SUCCESS:
            return action.project;
        default:
            return state;
    }
}