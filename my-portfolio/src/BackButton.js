import React, { Component } from 'react';
import "./BackButton.css";
class BackButton extends Component {
    constructor(props) {
        super(props);
        this.goBackYouMUST = this.goBackYouMUST.bind(this);
    }

    goBackYouMUST() {
        this.props.props.history.push('/');
    }

    render () {
        console.log(this.props);
        return (
            <div className="backbutton-wrapper">
                <button className="ZBackButtonActual" onClick={this.goBackYouMUST}>BACK</button>
            </div>
        )
    }
}

export default BackButton;