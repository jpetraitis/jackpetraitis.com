import * as types from './actionTypes';
import fetchProjectsApi from '../api/fetchProjectsApi';

function fetchProjectSuccess(payload) {
  return {type: types.FETCH_PROJECTS_SUCCESS, project: payload}
}

export function fetchStarterProject() {
  return function(dispatch) {
    return fetchProjectsApi.fetchStarterProject()(fetchprojectSuccess)
      .then(projectPayload => {
        dispatch(fetchProjectSuccess(projectPayload))
      })
      .catch(error => {
        console.error(error);
      }); 
  }
}

