import React, { Component } from 'react';
import "./BooksList.css";
import leaninBook from './img/finals/books/leanin.jpg';
import pragmaticBook from './img/finals/books/pragprog.jpg';
import designEverydayBook from './img/finals/books/thedesignofeverydaythings.jpg';
import dontMakeMeThinkBook from './img/finals/books/dontmakemethink.jpg';
import rocketSurgeryBook from './img/finals/books/rocketsurgery.jpg';
import javascriptPartsBook from './img/finals/books/javascriptgoodparts.jpg';
import automateTheBook from './img/finals/books/automatetheboring.png';
import hackersPaintersBook from './img/finals/books/hackersPainters.jpg';
import cyberEffectBook from './img/finals/books/cybereffect.jpg';

class BooksList extends Component {
    render() {
        return (
            <div id="Books" className="box-books">
                <div className="book-description">
                    <h3>
                    Books I Like
                    </h3>
                </div>
                <div className="book">
                    <p className=" books-currently-reading">
                        <img className="book-image" src={automateTheBook} alt="" />
                    </p>
                </div>
                <div className="book">
                    <p className=" books-already-read">
                    <img className="book-image" src={leaninBook} alt="" />
                    </p>
                </div>
                <div className="book">
                    <p className=" books-going-back">
                    <img className="book-image" src={pragmaticBook} alt="" />
                    </p>
                </div>
                <div className="book">
                    <p className=" books-already-read">
                    <img className="book-image" src={designEverydayBook} alt="" />
                    </p>
                </div>
                <div className="book">
                    <p className=" books-already-read">
                    <img className="book-image" src={dontMakeMeThinkBook} alt="" />
                    </p>
                </div>
                <div className="book">
                    <p className=" books-already-read">
                    <img className="book-image" src={rocketSurgeryBook} alt="" />
                    </p>
                </div>
                <div className="book">
                    <p className=" books-already-read">
                    <img className="book-image" src={javascriptPartsBook} alt="" />
                    </p>
                </div>
                <div className="book">
                    <p className=" books-already-read">
                    <img className="book-image" src={hackersPaintersBook} alt="" />
                    </p>
                </div>
                <div className="book">
                    <p className=" books-currently-reading">
                    <img className="book-image" src={cyberEffectBook} alt="" />
                    </p>
                </div>
                <div className="books-conclusion">
                    <div className="books-construction">
                        <p><em>My Books part here is currently under construction.</em></p>
                    </div>
                    <div className="extra-part" >
                        <div className="explainer-square currently-reading-purple">Purple = Currently Reading</div>
                        <div className="explainer-square already-read-burlywood">Burlywood = Already Read, and Loved It</div>
                        <div className="explainer-square always-going-back-to-green">Green = Always Going Back To Read It Again</div>
                    </div>
                </div>                
            </div>
        )
    }
}

export default BooksList;