import React, { Component } from 'react';
import MenuLinks from './MenuLinks';
import { withAuth } from '@okta/okta-react';
import './AppHeader.css';

export default withAuth(class AppHeader extends Component {
    constructor(props) {
        super(props);
        this.state = { authenticated: null };
        this.checkAuthentication = this.checkAuthentication.bind(this);
        this.checkAuthentication();
    }

    async checkAuthentication() {
        const authenticated = await this.props.auth.isAuthenticated();
        if (authenticated !== this.state.authenticated) {
          this.setState({ authenticated });
        }
    }

    componentDidUpdate() {
        this.checkAuthentication();
    }
    render() {
        return (
            <header className="App-header">
                <h1 className="App-title"><MenuLinks linkURL="/projects" linkText="Projects"></MenuLinks></h1>  
                <h1 className="App-title"><MenuLinks linkURL="/books" linkText="Books"></MenuLinks></h1>         
                { this.state.authenticated ? <span><a href="/logout" onClick={() => this.props.auth.logout()}>Logout</a></span>:<span className="App-login"><a href="/login">Login</a></span>}
                <i><h2 className="App-title center"><a href="/">jackpetraitis.com</a></h2></i>
            </header>
        );
    }
})