import React, { Component } from 'react';
import OktaAuth from '@okta/okta-auth-js';
import { withAuth } from '@okta/okta-react';
import "./LoginForm.css";

export default withAuth(class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sessionToken: null,
      username: '',
      password: ''
    }

    this.oktaAuth = new OktaAuth({ url: props.baseUrl });

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    this.oktaAuth.signIn({
      username: this.state.username,
      password: this.state.password
    })
    .then(res => this.setState({
      sessionToken: res.sessionToken
    }))
    .catch(err => console.log('Found an error', err));
  }

  handleUsernameChange(e) {
    this.setState({username: e.target.value});
  }

  handlePasswordChange(e) {
    this.setState({password: e.target.value});
  }

  render() {
    if (this.state.sessionToken) {
      this.props.auth.redirect({sessionToken: this.state.sessionToken});
      return null;
    }

    return (
      <div className="login-form">
        <form onSubmit={this.handleSubmit}>
            <span className="hover-div-user">
              <label className="input-label">Username:</label>
              <input
                id="username" type="text"
                value={this.state.username}
                onChange={this.handleUsernameChange} />
              </span>
            <span className="hover-div-pass">
              <label className="input-label">Password:</label>
              <input
                id="password" type="password"
                value={this.state.password}
                onChange={this.handlePasswordChange} />
            </span>
          <input id="submit" type="submit" value="Submit" />
          
          <div className="donthaveaccount">
              <h4>Don't have an account?</h4>
              <a className="signup-link" href="/signup">Sign Up</a>
          </div>
        </form>
      </div>
    );
  }
});