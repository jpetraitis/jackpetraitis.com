import React, { Component } from 'react';
import "./ProjectDetails.css";
import * as projects from "./reducers/initialState";
import BackButton from "./BackButton";

class ProjectDetails extends Component {
    
    render() {
      let woopsies = projects.default.projects;
      let lookinFerThisID = this.props.match.params.id;
      let found = woopsies.find(function(element) {
        return element.id === lookinFerThisID;
      });
      return (
        <div className="ProjectDetails">
          <BackButton props={this.props}/>
            <div className="all-container">
              <div className="image-container">
              {found.imgsrc}
                <div id="IMageDIV">real<img src={found.imgsrc} alt="this.props.projects[id].imgsrc"/></div>
              </div>
              <div className="details-container">
                <h2>Name: {found.name}</h2>
                <p><em>Short Details: {found.shortdetails}</em></p>
                <p><em>Tech Stack: {found.techstack}</em></p>
                <p>Github: {found.github}</p>
                <p>URL: <a href={found.url}>{found.url}</a></p>
                <p>Long Details: {found.longdetails}</p>
              </div>
            </div>
        </div>
      )
    }
}

export default ProjectDetails;
