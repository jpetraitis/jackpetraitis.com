# README

## Official website portfolio source code for jackpetraitis.com

*Written in ReactJS*
_Designed with plain old HTML and CSS_

8/29/2019
By Jack Petraitis

### SET UP
1. Install dependencies by running
```
    npm install
```
2. Run a build of the app by running
```
    npm run build
```
3. Start the app to test it by running
```
    npm run start
```

### DEPLOYING
1. Commit changes to repo
2. Every 5 minutes the changes will be looked for, and if picked up
3. Jenkins does its own setup of the project, builds it, and sends it off to AWS
4. After AWS S3 refreches, the CDN's stores will take a moment to catch up, force refresh and...
5. See your changes!